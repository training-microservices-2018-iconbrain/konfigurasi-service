package com.permana.brain.wilson.training.microservice.konfigurasiservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class KonfigurasiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KonfigurasiServiceApplication.class, args);
	}
}
